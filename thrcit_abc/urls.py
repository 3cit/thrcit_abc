"""thrcit_abc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from drf_yasg.generators import OpenAPISchemaGenerator

API_PATH = 'v1/'  # importar de alguma constants
schema_view = get_schema_view(
    info=openapi.Info(
        title="3cIT ABC API",
        default_version=API_PATH,
        description="API do serviço da 3cIT voltadas às instituições de ensino.",
        terms_of_service="https://google.com.br/q=3cit+termos",
        contact=openapi.Contact(email="3cittecnologia@gmail.com"),
        license=openapi.License(name="MIT License"),
    ),
    generator_class=OpenAPISchemaGenerator,
    public=True,
)

api_paths = [
    path('', include('autenticacao.api_urls')),
    path('', include('thrcit_logging.api_urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path(API_PATH, include(api_paths)),
    path('app/', include('autenticacao.urls')),
    re_path(
        r'^schema(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0),
        name="schema-json"
    ),
]
